# - Try to find libgpib
# Once done this will define
#  LIBGPIB_FOUND - System has libGPIB
#  LIBGPIB_DEFINITIONS - Compiler switches required for using libGPIB

FIND_PATH(LIBGPIB_INCLUDE_DIR ib.h
  HINTS /usr/include/gpib /usr/local/include/gpib )

FIND_LIBRARY(LIBGPIB_LIBRARY NAMES libgpib.so
  HINTS /usr/lib64 /usr/local/lib /usr/lib/x86_64-linux-gnu )

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBGPIB_FOUND to TRUE
# if all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(libgpib  DEFAULT_MSG
  LIBGPIB_LIBRARY LIBGPIB_INCLUDE_DIR)

#MARK_AS_ADVANCED(LIBGPIB_INCLUDE_DIR LIBGPIB_LIBRARY )
MARK_AS_ADVANCED(LIBGPIB_LIBRARY )

SET(LIBGPIB_LIBRARIES ${LIBGPIB_LIBRARY} )
#SET(LIBGPIB_INCLUDE_DIRS ${LIBGPIB_INCLUDE_DIR} )
