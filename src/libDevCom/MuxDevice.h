#ifndef MUXDEVICE_H
#define MUXDEVICE_H

#include <stdint.h>

/** \brief Interface for programmable multiplexers
 */
class MuxDevice
{
public:
  MuxDevice();
  virtual ~MuxDevice() =default;

  /** \brief Select output channel
   *
   * \param ch Channel to set for output
   */
  virtual void select(uint32_t ch) =0;
};

#endif // MUXDEVICE_H
