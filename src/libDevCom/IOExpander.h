#ifndef IOEXPANDER_H
#define IOEXPANDER_H

#include <cstdint>
#include <vector>
#include <memory>
#include <map>

class IOExpander
{
public:
  IOExpander() =default;
  virtual ~IOExpander() =default;


  // bit operations
  void setIO(uint32_t bit, bool output);
  void write(uint32_t bit, bool value);
  bool read(uint32_t bit);
  
  // block operations
  virtual uint32_t getIO() =0;
  virtual void setIO(uint32_t output) =0;
  virtual void write(uint32_t value) =0;
  virtual uint32_t read() =0;
};

#endif // IOEXPANDER_H
