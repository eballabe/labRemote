#include "PGA11x.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(PGA11x, MuxDevice)

PGA11x::PGA11x(std::shared_ptr<SPICom> com)
  : m_com(com)
{ }

void PGA11x::select(uint32_t ch)
{
  m_com->write_reg16(0x2A00 | ch&0xF);
}
