#ifndef POLYSCILM_H
#define POLYSCILM_H

#include <string>
#include <stdexcept>
#include <memory>

#include "Logger.h"
#include "TextSerialCom.h"
#include "IChiller.h"

//! \brief Object to interface with a PolyScience LM series chiller
/**
 * The TextSerialCom passed to this object should already be 
 * initialized, be set to the same baud rate as the chiller, 
 * and have the termination set to "\r".
 *
 * # Example
 *
 * ```
 * std::shared_ptr<TextSerialCom> com = 
 *   std::make_shared<TextSerialCom>("/dev/ttyUSB0",B9600);
 * com->setTermination("\r");
 * com->init();
 *
 * PolySciLM chiller(com);
 *
 * chiller.init();
 * chiller.setSetTemperature(15.0);
 * ```
 *
 * The operator's manual for these chillers can be found at 
 * https://www.polyscience.com/chillers/lm-series-benchtop-chiller-centrifugal-pump-0
 */
class PolySciLM : public IChiller
{
public:
  /**
   * \param com The serial communication interface connected
   *   to the chiller
   */ 
  PolySciLM();
  ~PolySciLM();

  //! Initialize the serial communication channel
  void init();
  //! Turn the chiller on
  void turnOn();
  //! Turn the chiller off
  void turnOff();
  //! Set the target temperature of the chiller
  /**
   * \param temp The target temperature in Celsius
   */
  void setTargetTemperature(float temp);
  //! Return the temperature that the chiller is set to in Celsius
  float getTargetTemperature();
  //! Return the current temperature of the chiller in Celsius
  float measureTemperature();
  //! Get the status of the chiller
  /**
   * \return true if chiller is in "run" mode, and false if 
   *   it's in "standby" mode
   */
  bool getStatus();
  //! Returns error fault codes
  /**
   * \return "00" if system OK, and an error code otherwise
   */ 
  std::string getFaultStatus();

private:
  //! True if conversion is necessary, false otherwise
  bool m_convert;
  
  //! Send a command to the chiller
  void command(const std::string& cmd);
  //! Get the temperature units used by the chiller
  /**
  * \return true for Celsius and false for Fahrenheit
  */
  bool getTemperatureUnit();
};

#endif
