add_library(Utils SHARED)
target_sources(Utils
  PRIVATE
  Logger.cpp
  ScopeLock.cpp
  )
target_include_directories(Utils PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
set(libUtils_FOUND TRUE PARENT_SCOPE)

set_target_properties(Utils PROPERTIES VERSION ${labRemote_VERSION_MAJOR}.${labRemote_VERSION_MINOR})
install(TARGETS Utils)
