# libPS 

Library that contain software to configure and control power supply
At each PS class corresponds a programming manual. 
If you need to use a PS that shares manufacturer and programming manual with an existing PS, you may only need to update the checkCompatibilityList to include your new model.

## Power supply registration
This library configures helper functions and registry for specific power supply implementations.
