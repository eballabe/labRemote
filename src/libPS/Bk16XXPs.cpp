#include "Bk16XXPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"
#include "ScopeLock.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Bk16XXPs)

Bk16XXPs::Bk16XXPs(const std::string& name): 
IPowerSupply(name, {}){} 

bool Bk16XXPs::ping()
{
  const std::vector<std::string>& result = command("GETD");
  return result.size()>0;
}

void Bk16XXPs::checkCompatibilityList()
{
  logger(logWARNING)<<"No checkCompatibilityList function can be implemented for Bk16kk PS. This implementation is valid only for 1687B and 1688B ";
}

void Bk16XXPs::reset()
{
  logger(logINFO)<<"No reset function can be implemented for Bk16kk PS ";
}

std::string Bk16XXPs::identify()
{
  return "";
}

void Bk16XXPs::turnOn(unsigned channel )
{
  command("SOUT0");
}

void Bk16XXPs::turnOff(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SOUT1");
}

void Bk16XXPs::setCurrentLevel(double curr, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  curr *= 10;
  int curr_int = (int)curr;
  if (curr_int < 100)
    command("CURR0" + std::to_string(curr_int));
  else
    command("CURR" + std::to_string(curr_int));
}

double Bk16XXPs::getCurrentLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  const std::vector<std::string>& ret = command("GETS");

  std::string volt_curr = ret[0];
  int volt_curr_int = stoi(volt_curr);
  int curr = volt_curr_int % 1000;
  return (double)curr / 10.0;
}



void Bk16XXPs::setCurrentProtect(double curr, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  curr *= 10;
  int curr_int = (int)curr;
  if (curr_int < 100)
    command("SOCP0" + std::to_string(curr_int));
  else
    command("SOCP" + std::to_string(curr_int));
}

double Bk16XXPs::getCurrentProtect(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  const std::vector<std::string>& ret = command("GOCP");
  const std::string& max_curr = ret[0];
  std::string current = max_curr.substr(0, max_curr.find("\r"));
  int curr_int = stoi(current);
  double curr = floor((double)curr_int / 10);
  return curr;
}

double Bk16XXPs::measureCurrent(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  const std::vector<std::string>& ret = command("GETD");
  const std::string& volt_curr = ret[0];
  int volt_curr_int = std::stoi(volt_curr);
  double curr = floor((double)(volt_curr_int % 100000) / 10);
  return curr / 100;
}

void Bk16XXPs::setVoltageLevel(double volt,unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  volt *= 10;
  int volt_int = (int)volt;
  if (volt_int < 100)
    command("VOLT0" + std::to_string(volt_int));
  else
    command("VOLT" + std::to_string(volt_int));
}

double Bk16XXPs::getVoltageLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  const std::vector<std::string>& ret = command("GETS");
  const std::string& volt_curr = ret[0];

  int volt_curr_int = stoi(volt_curr);
  double volt = floor((double)volt_curr_int / 1000);
  return volt / 10;
}

void Bk16XXPs::setVoltageProtect(double volt, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  volt *= 10;
  int volt_int = (int)volt;
  if (volt_int < 100)
    command("SOVP0" + std::to_string(volt_int));
  else
    command("SOVP" + std::to_string(volt_int));
}

double Bk16XXPs::getVoltageProtect(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");


  const std::vector<std::string>& ret = command("GOVP");
  const std::string& voltage = ret[0];
  int volt_int = stoi(voltage);
  double volt = floor((double)volt_int / 10);
  return volt;
}

double Bk16XXPs::measureVoltage(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  const std::vector<std::string>& ret = command("GETD");
  const std::string& volt_curr = ret[0];
  int volt_curr_int = stoi(volt_curr);
  double volt = floor((double)volt_curr_int / 100000);
  return volt / 100;
}

std::vector<std::string> Bk16XXPs::command(const std::string& cmd)
{
  ScopeLock lock(m_com);

  m_com->send(cmd);
  std::vector<std::string> ret;

  // Read until we see the last "OK"
  while(ret.size()==0 || ret.back()!="OK")
    {
      std::string response=m_com->receive();

      // Tokenize
      std::stringstream ss(response);
      std::string line;
      while(std::getline(ss, line, '\r'))
	ret.push_back(line);
    }
  return ret;
}
