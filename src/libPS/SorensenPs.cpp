#include "SorensenPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(SorensenPs)

SorensenPs::SorensenPs(const std::string& name) :
IPowerSupply(name, {"60-20DP"})
{ }

bool SorensenPs::ping()
{
  std::string result = m_com->sendreceive("*IDN?");
  return !result.empty();
}

void SorensenPs::reset()
{
  m_com->send("OPALL 0");
  m_com->send("*RST");

  if(!ping())
    throw std::runtime_error("No communication after reset.");
}

std::string SorensenPs::identify()
{
  std::string idn=m_com->sendreceive("*IDN?");
  return idn;
}

void SorensenPs::turnOn(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->send("OP"+std::to_string(channel)+" 1");
}

void SorensenPs::turnOff(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->send("OP"+std::to_string(channel)+" 0");
}

void SorensenPs::setCurrentLevel(double cur, unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->send("I" + std::to_string(channel)+" "+std::to_string(cur));
}

double SorensenPs::getCurrentLevel(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(m_com->sendreceive("I" + std::to_string(channel) + "?").substr(3));
}

void SorensenPs::setCurrentProtect(double maxcur, unsigned channel)
{
  setCurrentLevel(maxcur, channel);
}

double SorensenPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel(channel);
}

double SorensenPs::measureCurrent(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(m_com->sendreceive("I" + std::to_string(channel) + "O?").substr(0,5));
}

void SorensenPs::setVoltageLevel(double volt, unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->send("V" + std::to_string(channel)+" "+std::to_string(volt));
}

double SorensenPs::getVoltageLevel(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(m_com->sendreceive("V" + std::to_string(channel) + "?").substr(3));
}

void SorensenPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setVoltageLevel(maxvolt, channel);
}

double SorensenPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel(channel);
}

double SorensenPs::measureVoltage(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(m_com->sendreceive("V" + std::to_string(channel) + "O?").substr(0,5));
}
