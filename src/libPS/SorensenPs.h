#ifndef SORENSENPS_H
#define SORENSENPS_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

#include "SerialCom.h"

/** \brief Sorensen XPF 60-20DP
 *
 * Implementation for the Sorensen XPF 60-20DP power supply.
 *
 * [Programming Manual](http://www.sorensen.com/products/XPF/downloads/XPF_60-20DP_Operation_Manual_M370417-01_Rev_A.pdf)
 *
 * Other Sorensen power supplies might be supported too, but
 * have not been checked.
 */
class SorensenPs : public IPowerSupply
{
public:
  SorensenPs(const std::string& name);
  ~SorensenPs() =default;

  /** \name Communication
   * @{
   */

  virtual bool ping();

  virtual std::string identify();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();
  virtual void turnOn(unsigned channel);
  virtual void turnOff(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */
};

#endif

