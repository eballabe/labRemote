#include "RigolDP832.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(RigolDP832)


RigolDP832::RigolDP832(const std::string& name) :
SCPIPs(name, {"DP832"})
{ }


